import argparse
import logging
import time
import glob
import os
from os import urandom
import subprocess
from base64 import b64encode
import requests
import asyncio
import queue

import watchdog.events
import watchdog.observers

from ndn.app import NDNApp
from ndn.encoding import Name, Component, InterestParam
from ndn.security import KeychainDigest
from ndn_python_repo.clients import PutfileClient
from ndn.types import InterestNack, InterestTimeout, InterestCanceled, ValidationFailure
import ndn.utils


class FSWatchdog(watchdog.events.PatternMatchingEventHandler):
    """
    Event handler for file system watch dog. Whenever a new file is created,
    the on_modified event handler is invoked.
    """

    def __init__(self, ignore_folder, callback):
        """

        :param ignore_folder: Ignore changes that occur in the specified folder
        """
        self.ignore_folder = ignore_folder
        self.callback = callback
        # Set the patterns for PatternMatchingEventHandler
        watchdog.events.PatternMatchingEventHandler.__init__(self, patterns=['./*'],
                                                             ignore_directories=True, case_sensitive=False)

    def __handle_change(self, event):
        # If the change is in the folder ignore_path, ignore the change
        if self.ignore_folder:
            if self.ignore_folder in event.src_path:
                print(self.ignore_folder)
                print(event.src_path)
                return

        # Check if file is a hidden file
        if os.path.basename(event.src_path)[0] == '.':
            return

        time_ms = int(time.time() * 1000)
        logging.info("Watchdog received modified event - {} at {}.".format(event.src_path, time_ms))
        self.callback(event.src_path, time_ms)

    def on_created(self, event):
        """
        on_create handle provided by `watchdog` library
        """
        self.__handle_change(event)

    def on_modified(self, event):
        """
        on_modified handle provided by `watchdog` library
        """
        self.__handle_change(event)

    # def on_moved(self, event):
    #     """
    #     on_moved handle provided by `watchdog` library
    #     """
    #     self.__handle_change(event)


class FileUploader:
    """
    The FileUploader client monitors a directory with logfiles and whenever a logfile changes,
    the logfile is inserted into a repo.
    """

    def __init__(self, logfolder, nodename, repo_prefix, keydir, ignore_folder=None, monitor_interval=2):
        """

        :param logfolder: Folder to monitor
        :param nodename: Name identifying the node
        :param repo_prefix: Prefix of the NDN-Repo
        :param ignore_folder: Don't upload files in the specified folder
        :param monitor_interval: How frequently is the logfolder monitored
        """
        self.logfolder = logfolder
        self.nodename = nodename
        self.repo_prefix = repo_prefix
        self.keydir = keydir
        self.ignore_folder = ignore_folder
        self.monitor_interval = monitor_interval
        self.queue = queue.Queue()

    def start(self):
        """
        Start threads and NDN-Face
        :return:
        """
        self.runnning = True
        logging.info("Starting FileUploader threads")

        fs_watchdog = FSWatchdog(self.ignore_folder, self.add_changed_file)
        self.observer = watchdog.observers.Observer()
        self.observer.schedule(fs_watchdog, path=self.logfolder, recursive=True)
        self.observer.start()

        self.app = NDNApp()
        self.app.run_forever(after_start=self.__monitor_logfiles())

    def shutdown(self):
        logging.info("Shutdown FileUploader")
        self.runnning = False

        self.observer.join()

    def add_changed_file(self, changed_filename, timestamp):
        print(changed_filename)
        # This is called from the FSWatchdog thread. Just add to the queue.
        self.queue.put((changed_filename, timestamp))

    async def __monitor_logfiles(self):

        #####
        # Initialize PutFile Repo Client, needed for file insertion
        self.client = PutfileClient(app=self.app,
                                    prefix=Name.from_str("/{}/".format(self.nodename)),
                                    repo_name=Name.from_str(self.repo_prefix),
                                    keydir=self.keydir)
        #####
        # Monitor changes in logfile directory
        while self.runnning:
            logging.info("Checking for logfile changes")

            if not self.queue.empty():
                logging.debug("Files in logfolder changed")

                # Transfer to a local changed_files managed only by this thread.
                changed_files = []
                while not self.queue.empty():
                    (changed_filename, timestamp) = self.queue.get()

                    # Don't add the same file twice.
                    have_file = False
                    for changed_file in changed_files:
                        if changed_filename == changed_file[0]:
                            have_file = True
                            break
                    if not have_file:
                        changed_files.append((changed_filename, timestamp))

                upload_file_tasks = []
                for changedFile in changed_files:
                    upload_file_tasks.append(
                        asyncio.create_task(self._upload_file(changedFile[0], changedFile[1]))
                    )
                #####
                # Upload changed files to repo
                await asyncio.wait(upload_file_tasks, return_when=asyncio.ALL_COMPLETED)

            #####
            # Sleep a few seconds before checking for changes again
            await asyncio.sleep(self.monitor_interval)

    async def _upload_file(self, filename, timestamp):
        """
        Upload a given file with the given timestamp to the repo. The name in the repo will be
        /<nodename>/<filename>/<timestamp>
        :param filename:
        :param timestamp:
        :return:
        """

        str_name = requests.utils.quote("/{}/{}/{}".format(self.nodename, filename, timestamp))
        remote_name = Name.from_str(str_name)
        logging.info("Inserting file {} with name {}".format(filename, remote_name))

        # Todo: The uploaded data needs to be encrypted

        # Todo: The uploaded files need to be signed
        packets_inserted = await self.client.insert_file(file_path=filename,
                                      name_at_repo=remote_name,
                                      segment_size=2000,
                                      freshness_period=1000,
                                      cpu_count=1)
        if packets_inserted > 0:
            logging.info("File {} successfully inserted".format(filename))
        else:
            logging.warning("No packets were inserted")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Logfile uploader that monitors log files in a folder and inserts them'
                                                 'into a pyhton-ndn-repo.')
    parser.add_argument('--logfolder', dest='logfolder', type=str, default='log/',
                        help='logfolder to monitor')
    parser.add_argument('--ignore', dest='ignore', type=str, default='current',
                        help='Folder to ignore [current]')
    parser.add_argument('--nodename', dest='nodename', type=str, default='node_a',
                        help='Name of the current node')
    parser.add_argument('--repoprefix', dest='repoprefix', type=str, default='/testrepo/',
                        help='Prefix the Repo is reachable')
    parser.add_argument('--keydir', dest='keydir', type=str, default='key/',
                        help='dir of identity and content encryption key')
    logging.basicConfig(level=logging.INFO)

    args = parser.parse_args()

    uploader = FileUploader(args.logfolder, args.nodename, args.repoprefix, args.keydir, ignore_folder=args.ignore)
    uploader.start()
