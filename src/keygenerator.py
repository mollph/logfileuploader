import argparse
from os import urandom, path, makedirs
import subprocess
from base64 import b64encode

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Identity key and encryption key generator for the Logfile uploader.')
    parser.add_argument('--nodename', dest='nodename', type=str, default='node_a',
                        help='Name of the current node')

    args = parser.parse_args()

    # generate idenity key pair
    subprocess.run(['ndnsec-key-gen', '/' + args.nodename, '-n'], stdout=subprocess.PIPE)
    p = subprocess.run(['ndnsec-export', '/' + args.nodename, '-P', '1234'], stdout=subprocess.PIPE)
    with open('./key/' + args.nodename + '.ndnkey', 'wb') as f:  
        f.write(p.stdout)
    
    # generate content encryption key
    aes_key_bits = urandom(16)
    aes_key_name = '/' + args.nodename + '/CK' + '\n' + str(b64encode(urandom(4)))
    content_key_dir = path.dirname('./key/' + args.nodename + '-aes.key')
    if content_key_dir:
        makedirs(content_key_dir, exist_ok=True)
    with open('./key/' + args.nodename + '-aes.key', 'wb') as f:
        f.write(str(aes_key_name + '\n').encode() + b64encode(aes_key_bits))

